#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from time import sleep
from datetime import date, datetime, timedelta
from os import path

# ------------------------------------------------------------------------ #
# Привет!
# Этот скрипт парсит файл с днями рождений и на выходе генерирует HTML страницу,
# которую ты можешь запихать в iframe на своем любимом сайтике.
# Кстати, в этой страничке упорядоченно отображаются 5 ближайших дней рождений.
#
# Работает этот скрипт как демон, то есть внутри работет бесконечный цикл, сверяющий
# текущее время с тем, что указано в глобальной переменной UPDATE_TIME, и если они совпадают -
# запускается главная функция.
#
# INPUT_FILE должен иметь вид (для примера):
# Январь
# 01/01/2020  Алексеев Алексей Алексеевич
# 02/01/2020  Андреев Андрей Андреевич
# Февраль
# 03/02/2020  Иванов Иван Иванович
# 04/02/2020  Петров Петр Петрович
# и т.д.
#
# Если строку в файле с днями рождений нужно закомментировать -
# поставь в её начале точку с запятой ";" - она будет игнорироваться
#
# ВАЖНО! В файле между датой рождения и ФИО ОБЯЗАТЕЛЬНО ставятся 2 пробела!
# Иначе он не прочитается и сгенерируется пустой HTML
#
# P.S. Учти, я написал этот скрипт за прару часов, так что желаю удачи с ним разобраться ;-)
# ------------------------------------------------------------------------ #

UPDATE_TIME = "01:02:03"  # Время обновления файла out.html

INPUT_FILE = "events.txt"  # Файл с днями рождений (создается вручную)
OUTPUT_FILE = "out.html"  # Файл на выходе
DASH_LINE = "-" * 50  # Линия для кратоты отображения в консоли


def read_from_file(event_file):  # Открываю и читаю файл с днями рождений
    if path.exists(event_file):
        with open(event_file, "r", encoding="utf-8") as f:
            lines = f.read().splitlines()

        birth_list = []

        for line in lines:
            # Если между датой и фио 2 пробела и строка не начинается с ";"
            if len(line.split("  ")) >= 2 and str(line)[0] != ";":
                birth_date = line.split('  ')[0]  # Делю на дату
                birth_name = line.split('  ')[1]  # И на Ф.И.О.
                birth_list.append([birth_date.replace('/', '.')[0:5], birth_name])  # Меняю "/" на точки, для красоты

        return birth_list

    else:
        return []  # возврат пустого списка если файла не существует


def write_in_file(output_file):  # Записываю все изменения в выходной файл
    with open(output_file, "w", encoding="utf-8") as out_f:
        for line in generate_html():
            out_f.write(line)


def upcoming_birthdays():  # Ищу 5 ближайших дней рождений
    dates_list = read_from_file(INPUT_FILE)

    if len(dates_list) == 0:
        print("%s\n[-] Ошибка! Файл \"%s\" пустой или не найден." % (DASH_LINE, INPUT_FILE))
        print("[-] Создан пустой файл \"%s\"\n%s" % (OUTPUT_FILE, DASH_LINE))
        input("Для выхода нажмите любую клавишу...")
        exit(1)

    day = date.today().strftime("%d")
    month = date.today().strftime("%m")

    proximate_birthday_index = 0  # Индекс ближайшей даты рождения
    birthday_index_remain = 0  # для отсчета с нового года
    birth_list_for_file = []  # Возвращаемый список

    # Ищу индекс ближайшего дня рождения
    for prx_index in range(len(dates_list)):
        day_from_list = dates_list[prx_index][0].split('.')[0]
        month_from_list = dates_list[prx_index][0].split('.')[1]

        # Ищу ближайшую дату рождения к сегодняшней и запоминаю индекс этой строки
        if int(day_from_list) >= int(day) and int(month_from_list) >= int(month):
            proximate_birthday_index = prx_index
            break

    # Массив будет состоять из 5 ближайших дней рождений,
    birthday_index_end = proximate_birthday_index + 5

    # ВНИМАНИЕ! Тут жесткий кусок кода, делал его в спешке, но все работает идеально.
    # Его возможно оптимизировать, но я не стал.
    # Если у тебя много времени - попробуй сам :-P

    # Если конечный индекс больше длинны массива (если например ближайший день рождения 30 декабря),
    # необходимо для переноса на следующий год
    if birthday_index_end > len(dates_list):
        birthday_index_remain = birthday_index_end - len(
            dates_list)  # Запоминаю остаток для прохода по списку с начала года
        birthday_index_end = len(dates_list)

        # Обновление массива дней рождений
    for brh_index in range(proximate_birthday_index, birthday_index_end):
        birth_list_for_file.append("%s %s" % (dates_list[brh_index][0], dates_list[brh_index][1]))

    # Если есть перенос на следующий год
    if birthday_index_remain > 0:
        for new_year_brh in range(birthday_index_remain):
            birth_list_for_file.append("%s %s" % (dates_list[new_year_brh][0], dates_list[new_year_brh][1]))

    return birth_list_for_file


def generate_html():  # Генерирую HTML страницу (говнокод)
    brh = upcoming_birthdays()
    date_now = date.today().strftime("%d.%m")

    div_birthday = "<div class=\"birthday\">\n"
    div_today = "\n<div class=\"today\">\n"
    div_close = "</div>\n"

    styles = """<style type="text/css">
    .birthday {
        padding: 1px 0px 0px 0px;
        color: steelblue;
        font-family: sans-serif;
        font-style: italic;
        font-size: 15px;
    }

    .today {
        color: firebrick;
        font-weight: bold;
        border-bottom: 1px solid rgba(70, 130, 180, 0.3);
    }
    </style>"""

    html_head = """<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>birthday</title>\n
    %s
</head>
<body>\n""" % styles

    today_brh = [brh[i] for i in range(len(brh)) if brh[i].split(" ")[0] == date_now]  # Если день рождения сегодня
    next_brh = [brh[i] for i in range(len(brh)) if brh[i].split(" ")[0] != date_now]  # Последующие дни рождения

    today_brh_str = ""
    next_brh_str = ""

    for b in today_brh:
        today_brh_str += b + "<br>\n"

    for n in next_brh:
        next_brh_str += n + "<br>\n"

    top = html_head + div_birthday + "Ближайшие дни рождения:<br><br>"
    center = div_today + today_brh_str + div_close + "<br>" + next_brh_str
    bottom = div_close + "\n</body>\n</html>"

    ready_brh = top + center + bottom

    return ready_brh


def update_html(update_html_time):  # Собственно сам цикл
    print("%s\n[+] Поиск пяти ближайших дней рождений [+]" % DASH_LINE)
    print("[+] Программа запущена %s в %s" % (date.today().strftime("%d.%m.%Y"), datetime.now().strftime("%H:%M:%S")))

    write_in_file(OUTPUT_FILE)  # Создаю файл при первом запуске

    if path.exists(OUTPUT_FILE):
        print("[+] Файл \"%s\" создан.\n%s" % (OUTPUT_FILE, DASH_LINE))

    while True:
        today = date.today()
        tomorrow = today + timedelta(days=1)
        flow_time = datetime.now().strftime("%H:%M:%S")

        print("\r" + "[~] Текущие дата/время " + today.strftime("%d.%m.%Y - ") + flow_time +
              " | Следующая проверка: %s - %s " % (tomorrow.strftime("%d.%m.%Y"), update_html_time), end=' ')

        if flow_time == update_html_time:
            write_in_file(OUTPUT_FILE)

            print("\r[+] " + today.strftime("%d.%m.%Y - ") + flow_time, end=' | ')

            print("Файл \"%s\" создан.\n%s" % (OUTPUT_FILE, DASH_LINE)) if path.exists(OUTPUT_FILE) \
                else "[-] Файл не найден! Необходима проверка.\n"

            sleep(2)


if __name__ == "__main__":
    update_html(UPDATE_TIME)  # ПОЕХАЛИ!
